import logo2 from "./img/logo2.png";

import { Donations } from "./sections/Donations";
import { Infos } from "./sections/Infos";
import { Heroes } from "./sections/Heroes";
import { Footer } from "./sections/Footer";

import { donations, heroes, volunteers } from "./data";

function App() {
  return (
    <div className="container">
      <div className="header flex center column">
        <img className="header__logo" src={logo2} />
        <h1 className="header__title primary">Dia das Crianças</h1>
        <h2 className="header__text secondary padding-side align-center">
          Juntos nós fazemos a felicidade de muitas crianças!
        </h2>
      </div>
      <section className="section">
        <h3 className="align-center h3">Arrecadações 😍</h3>
        {donations.map((d) => (
          <Donations
            key={d.id}
            icon={d.icon}
            title={d.title}
            subtitle={d.subtitle}
            listItems={d.listItems}
            items={d.items}
          />
        ))}
      </section>
      <Infos />
      <Heroes heroes={heroes} />
      <Heroes heroes={volunteers} />
      <Footer />
    </div>
  );
}

export default App;

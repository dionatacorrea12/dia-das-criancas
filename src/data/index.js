export const donations = [
  {
    id: 1,
    icon: "🧸",
    title: "Brinquedos",
    subtitle: "",
    items: [
      {
        id: 1,
        name: "Brinquedos meninos",
        goal: 125,
        reached: 0,
      },
      {
        id: 2,
        name: "Brinquedos meninas",
        goal: 125,
        reached: 0,
      },
      
    ],
  },
  {
    id: 2,
    icon: "🍭",
    title: "Guloseimas",
    subtitle:
      "Essas guloseimas serão usadas para fazer os kits que cada criança receberá.",
    items: [
      {
        id: 1,
        name: "Bala de Goma (unidade)",
        goal: 220,
        reached: 0,
      },
      {
        id: 2,
        name: "Pirulitos (pacote 400g ou 500g)",
        goal: 30,
        reached:0,
      },
      {
        id: 3,
        name: "Pipoca doce",
        goal: 240,
        reached: 0,
      },
      {
        id: 4,
        name: "Balas (pacote)",
        goal: 30,
        reached: 0,
      },
      {
        id: 5,
        name: "Chiclete (caixa 50un)",
        goal: 10,
        reached: 0,
      },
      {
        id: 6,
        name: "Bom Bom (unidade)",
        goal: 480,
        reached: 0,
      },
      {
        id: 7,
        name: "Paçoca (unidade embalada individual)",
        goal: 240,
        reached: 0,
      },
      {
        id: 8,
        name: "Chocolates",
        goal: 240,
        reached: 0,
      },
      
    ],
  },
  {
    id: 3,
    icon: "🍔",
    title: "Alimentos",
    subtitle:
      "Alimentos que serão servidos no evento: pipoca e suco, e para levar para casa os cachorros quentes.",
    items: [
      {
        id: 1,
        name: "Pipoca (kg)",
        goal: 6,
        reached: 0,
      },
      {
        id: 2,
        name: "Suco Tang Laranja (caixa)",
        goal: 3,
        reached: 0,
      },
      {
        id: 3,
        name: "Pão Massinha (tamanho normal)",
        goal: 500,
        reached: 0,
      },
      {
        id: 4,
        name: "Salsicha (Kg)",
        goal: 10,
        reached: 0,
      },
      {
        id: 5,
        name: "Molho de Tomate (kg)",
        goal: 2,
        reached: 0,
      },
      {
        id: 6,
        name: "Batata Palha (kg)",
        goal: 2,
        reached: 0,
      },
      {
        id: 8,
        name: "Lata grande de milho (kg)",
        goal: 1,
        reached: 0,
      },
      {
        id: 9,
        name: "Lata grande de ervilha (kg)",
        goal: 1,
        reached: 0,
      },
      {
        id: 10,
        name: "Cebola (kg)",
        goal: 1,
        reached: 0,
      },
      {
        id: 11,
        name: "Tomate (kg)",
        goal: 1,
        reached: 0,
      },
      {
        id: 12,
        name: "Garaffa de azeite(unidade) ",
        goal: 6,
        reached: 0,
      },
      {
        id: 13,
        name: "Sal (kg) ",
        goal: 1,
        reached: 0,
      },
    ],
  },
  {
    id: 4,
    icon: "🍽 ",
    title: "Ajuda para as Famílias",
    subtitle:
      "As cestas básicas serão sorteadas para as famílias.",
    items: [
      {
        id: 1,
        name: "Cesta Básica",
        goal: 50,
        reached: 0,
      },
      
    ],
  },
  {
    id: 5,
    icon: "🛠️",
    title: "Diversos",
    subtitle: "",
    items: [
      {
        id: 1,
        name: "Embalagens plástica para cachorro quente",
        goal: 500,
        reached: 0
      },
      {
        id: 2,
        name: "Balão (pacote 50 unidades)",
        goal: 10,
        reached: 0,
      },
      {
        id: 3,
        name: "Embalagem de pipoca",
        goal: 1000,
        reached: 0,
      },
      {
        id: 4,
        name: "Sacos para guloseimas",
        goal: 250,
        reached: 0,
      },
      {
        id: 5,
        name: "Fitas mimosas (unidade)",
        goal: 2,
        reached: 0,
      },
      {
        id: 6,
        name: "Durex(unidade)",
        goal: 10,
        reached: 0,
      },
      {
        id: 7,
        name: "Rolo de papel de presente(unidade)",
        goal: 3,
        reached: 0,
      },
      {
        id: 8,
        name: "Tinta spray de cabelo(unidade)",
        goal: 8,
        reached: 0,
      },
    ],
  },
  {
    id: 6,
    icon: "💰",
    title: "Doações em dinheiro",
    subtitle:
      "Doações em dinheiro serão utilizados para compra de alguns dos items acima.",
    items: [
      {
        id: 1,
        name: "Doação em Dinheiro",
        isMoney: "0,00",
      },
    ],
  },
  {
    id: 7,
    icon: "💰",
    title: "Compras usando as doações em dinheiro",
    subtitle: "",
    listItems: [
       {
         id: 1,
         name: "Despesas com embalagens individuais.",
         when: "",
         value: "",
       },
        
    ],
  },
];

export const heroes = {
  title: "HERÓIS QUE JÁ DOARAM 🦸🏿‍♂️🦸🏽‍♀️",
  items: [
    {
      id: 1,
      user: "https://www.facebook.com/filipe.chaves.90",
      photo: "fernanda.jpg",
    },
    
    
    
  ],
};

export const volunteers = {
  title: "VOLUNTÁRIOS 💪 🙌",
  items: [
    
    {
      id: 1,
      user: "https://www.facebook.com/profile.php?id=100004641792420",
      photo: "fernanda.jpg",
    },
    
         
  ],
};

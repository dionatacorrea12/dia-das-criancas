import React from "react";

const Icon = ({ delay = 0 }) => (
  <svg
    style={{ animationDelay: "0ms" }}
    viewBox="0 0 100 100"
    xmlns="http://www.w3.org/2000/svg"
    xmlSpace="preserve"
  >
    <circle cx={50} cy={50} r={40} />
  </svg>
);

export const Heroes = ({ heroes }) => (
  <section class="section">
    <h2 class="align-center h3">{heroes.title}</h2>
    <div class="heroes">
      {heroes.items.map((item, index) => (
        <a href={item.user} class="circle" target="_blank" key={item.id}>
          <Icon delay={index * 100} />
          <img src={`/images/${item.photo}`} alt={item.user} />
        </a>
      ))}
    </div>
  </section>
);
